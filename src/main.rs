extern crate clap;
use clap::App;
use regex::Regex;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Error;
use std::io::Lines;
use std::iter::Chain;

struct FileIterator {
    finished_preamble: bool,
    lines: Lines<BufReader<File>>,
}

impl FileIterator {
    fn new(infile: File) -> FileIterator {
        let reader = BufReader::new(infile);
        let lines = reader.lines();
        FileIterator {
            finished_preamble: false,
            lines: lines,
        }
    }
}

impl Iterator for FileIterator {
    type Item = Result<String, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        while !self.finished_preamble {
            if self
                .lines
                .next()
                .expect("Could not find the end of the preamble, are you sure you added a comment?")
                .unwrap()
                .to_lowercase()
                .contains("preamble done")
            {
                self.finished_preamble = true;
            }
        }

        let line = self.lines.next();
        let text = line.expect("Could not find postamble comment.").unwrap();
        let layer_re = Regex::new(r"^\s*;\s*LAYER:([1-9]+)\s*$").unwrap();
        if text.to_lowercase().contains("postamble") || layer_re.is_match(&text) {
            None
        } else {
            Option::Some(Result::Ok(text))
        }
    }
}

fn read_replaced_file(
    file: File,
) -> Chain<Chain<Lines<BufReader<File>>, FileIterator>, Lines<BufReader<File>>> {
    let iterator = FileIterator::new(file);

    let preamble_lines =
        BufReader::new(File::open("gcode/preamble.gcode").expect("Could not open preamble file."))
            .lines();

    let postamble_lines = BufReader::new(
        File::open("gcode/postamble.gcode").expect("Could not open postamble_lines file."),
    )
    .lines();

    return preamble_lines.chain(iterator).chain(postamble_lines);
}

fn sanitize_gcode(file: File) {
    let chain = read_replaced_file(file).filter(|x| {
        !Regex::new(r"^(M104|M109|M140|M190|M105|M106|M107)( .*)?$")
            .unwrap()
            .is_match(x.as_ref().unwrap())
    });
    for line in chain {
        println!("{}", line.unwrap());
    }
}

fn main() {
    let matches = App::new("sanitize_gcode")
        .version("0.1")
        .about("...sanitizes Gcode.")
        .author("Two Guys")
        .args_from_usage(
            "
            <INPUT>                   'Sets the input file to use'
            ",
        )
        .get_matches();

    let filename = matches.value_of("INPUT").unwrap();
    println!("Filename: {}", filename);

    let file = File::open(filename).expect("Could not open input file.");
    println!("Sanitizing...");
    sanitize_gcode(file);
    println!("Done.");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_end_to_end() -> Result<(), String> {
        let input_file =
            File::open("test_files/test_input.gcode").expect("Could not open input file.");
        let expected_output_file = File::open("test_files/test_expected_output.gcode")
            .expect("Could not open input file.");

        sanitize_gcode(input_file);
        Result::Ok(())
    }
}
