; Postamble.
G91 ; Set to relative positioning
G1 Z30 F1200 ; Lift
G90 ; Set to absolute again
G0 X0 Y180 ; Move to home and forward
M84 ; Disable steppers
M106 S0 ; Turn fan off
M82 ;absolute extrusion mode
