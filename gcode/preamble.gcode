; Stavros' config.
G28 ;Home
M82 ; Absolute extrusion mode
G90 ; Set to absolute positioning
G1 X70 Y20 Z20 F3000
G1 Z0 F1000  ; Move the pen as low as it will go, so it touches the bed.
G1 Z15.0 F6000 ;Move the platform to 15mm
G92 E0

; Preamble done.
